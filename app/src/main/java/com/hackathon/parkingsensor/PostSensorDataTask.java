package com.hackathon.parkingsensor;

import android.os.AsyncTask;

import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

public class PostSensorDataTask extends AsyncTask<String, Void, Void> {

    @Override
    protected Void doInBackground(String... strings) {

        System.out.println("Do background task : " + strings[0]);
        sendRequestToServer(strings[0]);
        return null;

    }

    public void sendRequestToServer(String postData){
        try{
            URL url = new URL("http://10.3.101.251:8081/parking/sensor");
            System.out.println("URL : " + url);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(15000 );
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setRequestProperty("Content-Type", "application/json");


            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            System.out.println("POST Data  : " + postData);
            writer.write(postData);

            writer.flush();
            writer.close();
            os.close();

            int responseCode=conn.getResponseCode();
            System.out.println("Response Code: " + responseCode);

            if (responseCode == HttpsURLConnection.HTTP_OK) {

                BufferedReader in=new BufferedReader(new
                        InputStreamReader(
                        conn.getInputStream()));

                StringBuffer sb = new StringBuffer("");
                String line="";

                while((line = in.readLine()) != null) {

                    sb.append(line);
                    break;
                }

                in.close();
                System.out.println(sb.toString());

            }
            else {
                System.out.println("false : "+responseCode);
            }
        }
        catch (Exception exception){
            System.out.println("Error while connecting :: " + exception);
        }

    }
}
