package com.hackathon.parkingsensor;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

    TextView ProximitySensor, data;
    SensorManager mySensorManager;
    Sensor myProximitySensor;
    String entityId = "13656540102f477a9f6e25dd137d6d53";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ProximitySensor = (TextView) findViewById(R.id.proximitySensor);
        data = (TextView) findViewById(R.id.data);
        mySensorManager = (SensorManager) getSystemService(
                Context.SENSOR_SERVICE);
        myProximitySensor = mySensorManager.getDefaultSensor(
                Sensor.TYPE_PROXIMITY);
        if (myProximitySensor == null) {
            ProximitySensor.setText("No Proximity Sensor!");
        } else {
            mySensorManager.registerListener(proximitySensorEventListener,
                    myProximitySensor,
                    SensorManager.SENSOR_DELAY_NORMAL);
        }
    }
    SensorEventListener proximitySensorEventListener
            = new SensorEventListener() {
        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
            System.out.println("on accuracy changed " + accuracy);
        }
        @Override
        public void onSensorChanged(SensorEvent event) {

           String postDataFalse = "{\"entityId\":\"13656540102f477a9f6e25dd137d6d53\",\"available\":\"false\"}";
           String postDataTrue = "{\"entityId\":\"13656540102f477a9f6e25dd137d6d53\",\"available\":\"true\"}";

            if (event.sensor.getType() == Sensor.TYPE_PROXIMITY) {
                if (event.values[0] <= 50) {
                    data.setText("Near");
                    System.out.println("Sensor value Near : " + event.values[0]);
                    new PostSensorDataTask().execute(postDataFalse);

                } else {
                    data.setText("Away");
                    System.out.println("Sensor value Away : " + event.values[0]);
                    new PostSensorDataTask().execute(postDataTrue);

                }
            }
        }
    };





}
